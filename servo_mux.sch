EESchema Schematic File Version 2
LIBS:VincentDavoust
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:servo_mux-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "13 nov 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74HC164 U0
U 1 1 564593B3
P 3150 3800
F 0 "U0" H 3000 4850 60  0000 C CNN
F 1 "74HC164" H 3150 3800 60  0000 C CNN
F 2 "~" H 3150 3800 60  0000 C CNN
F 3 "~" H 3150 3800 60  0000 C CNN
	1    3150 3800
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K0
U 1 1 564593EA
P 6050 1000
F 0 "K0" V 6000 1000 50  0000 C CNN
F 1 "CONN_3" V 6100 1000 40  0000 C CNN
F 2 "~" H 6050 1000 60  0000 C CNN
F 3 "~" H 6050 1000 60  0000 C CNN
	1    6050 1000
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K1
U 1 1 564593F7
P 6050 1450
F 0 "K1" V 6000 1450 50  0000 C CNN
F 1 "CONN_3" V 6100 1450 40  0000 C CNN
F 2 "~" H 6050 1450 60  0000 C CNN
F 3 "~" H 6050 1450 60  0000 C CNN
	1    6050 1450
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K2
U 1 1 564593FD
P 6050 1850
F 0 "K2" V 6000 1850 50  0000 C CNN
F 1 "CONN_3" V 6100 1850 40  0000 C CNN
F 2 "~" H 6050 1850 60  0000 C CNN
F 3 "~" H 6050 1850 60  0000 C CNN
	1    6050 1850
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K3
U 1 1 56459403
P 6050 2250
F 0 "K3" V 6000 2250 50  0000 C CNN
F 1 "CONN_3" V 6100 2250 40  0000 C CNN
F 2 "~" H 6050 2250 60  0000 C CNN
F 3 "~" H 6050 2250 60  0000 C CNN
	1    6050 2250
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K4
U 1 1 56459409
P 6050 2650
F 0 "K4" V 6000 2650 50  0000 C CNN
F 1 "CONN_3" V 6100 2650 40  0000 C CNN
F 2 "~" H 6050 2650 60  0000 C CNN
F 3 "~" H 6050 2650 60  0000 C CNN
	1    6050 2650
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K5
U 1 1 5645940F
P 6050 3100
F 0 "K5" V 6000 3100 50  0000 C CNN
F 1 "CONN_3" V 6100 3100 40  0000 C CNN
F 2 "~" H 6050 3100 60  0000 C CNN
F 3 "~" H 6050 3100 60  0000 C CNN
	1    6050 3100
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K6
U 1 1 56459415
P 6050 3500
F 0 "K6" V 6000 3500 50  0000 C CNN
F 1 "CONN_3" V 6100 3500 40  0000 C CNN
F 2 "~" H 6050 3500 60  0000 C CNN
F 3 "~" H 6050 3500 60  0000 C CNN
	1    6050 3500
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K7
U 1 1 5645941B
P 6050 3900
F 0 "K7" V 6000 3900 50  0000 C CNN
F 1 "CONN_3" V 6100 3900 40  0000 C CNN
F 2 "~" H 6050 3900 60  0000 C CNN
F 3 "~" H 6050 3900 60  0000 C CNN
	1    6050 3900
	1    0    0    -1  
$EndComp
Text GLabel 5700 900  0    60   Input ~ 0
GND
Text GLabel 5700 1350 0    60   Input ~ 0
GND
Text GLabel 5700 1750 0    60   Input ~ 0
GND
Text GLabel 5700 2150 0    60   Input ~ 0
GND
Text GLabel 5700 2550 0    60   Input ~ 0
GND
Text GLabel 5700 3000 0    60   Input ~ 0
GND
Text GLabel 5700 3400 0    60   Input ~ 0
GND
Text GLabel 5700 3800 0    60   Input ~ 0
GND
Text GLabel 5700 3900 0    60   Input ~ 0
VCC
Text GLabel 5700 3500 0    60   Input ~ 0
VCC
Text GLabel 5700 3100 0    60   Input ~ 0
VCC
Text GLabel 5700 2650 0    60   Input ~ 0
VCC
Text GLabel 5700 2250 0    60   Input ~ 0
VCC
Text GLabel 5700 1850 0    60   Input ~ 0
VCC
Text GLabel 5700 1450 0    60   Input ~ 0
VCC
Text GLabel 5700 1000 0    60   Input ~ 0
VCC
Text GLabel 5700 1100 0    60   Input ~ 0
CMD0
Text GLabel 5700 1550 0    60   Input ~ 0
CMD1
Text GLabel 5700 1950 0    60   Input ~ 0
CMD2
Text GLabel 5700 2350 0    60   Input ~ 0
CMD3
Text GLabel 5700 2750 0    60   Input ~ 0
CMD4
Text GLabel 5700 3200 0    60   Input ~ 0
CMD5
Text GLabel 5700 3600 0    60   Input ~ 0
CMD6
Text GLabel 5700 4000 0    60   Input ~ 0
CMD7
Text GLabel 3650 2900 2    60   Output ~ 0
CMD0
Text GLabel 3650 3000 2    60   Output ~ 0
CMD1
Text GLabel 3650 3100 2    60   Output ~ 0
CMD2
Text GLabel 3650 3200 2    60   Output ~ 0
CMD3
Text GLabel 3650 3300 2    60   Output ~ 0
CMD4
Text GLabel 3650 3400 2    60   Output ~ 0
CMD5
Text GLabel 3650 3500 2    60   Output ~ 0
CMD6
Text GLabel 3650 3600 2    60   Output ~ 0
CMD7
Text GLabel 2650 3700 0    60   Input ~ 0
GND
Text GLabel 2650 2850 0    60   Input ~ 0
V1
Text GLabel 1300 3350 2    60   Output ~ 0
GND
Wire Wire Line
	2650 3150 2650 3050
Wire Wire Line
	2650 3050 1300 3050
Wire Wire Line
	2650 3350 2100 3350
Wire Wire Line
	2650 3500 1975 3500
Wire Wire Line
	1300 3250 2100 3250
$Comp
L CONN_2 P0
U 1 1 564595B6
P 2175 1100
F 0 "P0" V 2425 1200 40  0000 C CNN
F 1 "CONN_2" H 2225 1100 40  0000 C CNN
F 2 "~" H 2175 1100 60  0000 C CNN
F 3 "~" H 2175 1100 60  0000 C CNN
	1    2175 1100
	0    -1   -1   0   
$EndComp
$Comp
L C C0
U 1 1 564595DF
P 2175 1725
F 0 "C0" H 2175 1825 40  0000 L CNN
F 1 "C" H 2181 1640 40  0000 L CNN
F 2 "~" H 2213 1575 30  0000 C CNN
F 3 "~" H 2175 1725 60  0000 C CNN
	1    2175 1725
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2075 1450 1975 1450
Wire Wire Line
	1975 1450 1975 1725
Wire Wire Line
	2275 1450 2375 1450
Wire Wire Line
	2375 1450 2375 1725
$Comp
L C C1
U 1 1 5645960A
P 2075 2525
F 0 "C1" H 2075 2625 40  0000 L CNN
F 1 "C" H 2081 2440 40  0000 L CNN
F 2 "~" H 2113 2375 30  0000 C CNN
F 3 "~" H 2075 2525 60  0000 C CNN
	1    2075 2525
	-1   0    0    1   
$EndComp
Text GLabel 2075 2725 0    60   Input ~ 0
GND
Text GLabel 2075 2325 0    60   Input ~ 0
V1
Text GLabel 1975 1725 0    60   Output ~ 0
GND
Text GLabel 2375 1725 2    60   Output ~ 0
VCC
$Comp
L CONN_5 P1
U 1 1 564596A5
P 900 3150
F 0 "P1" V 850 3150 50  0000 C CNN
F 1 "CONN_5" V 950 3150 50  0000 C CNN
F 2 "~" H 900 3150 60  0000 C CNN
F 3 "~" H 900 3150 60  0000 C CNN
	1    900  3150
	-1   0    0    1   
$EndComp
Text GLabel 1300 2950 2    60   Output ~ 0
V1
$Comp
L CONN_2 J0
U 1 1 56459737
P 1025 2150
F 0 "J0" V 1275 2250 40  0000 C CNN
F 1 "CONN_2" H 1075 2150 40  0000 C CNN
F 2 "~" H 1025 2150 60  0000 C CNN
F 3 "~" H 1025 2150 60  0000 C CNN
	1    1025 2150
	-1   0    0    1   
$EndComp
Text GLabel 1375 2050 2    60   Input ~ 0
VCC
Text GLabel 1375 2250 2    60   Output ~ 0
V1
Wire Wire Line
	2100 3250 2100 3350
Wire Wire Line
	1975 3500 1975 3150
Wire Wire Line
	1975 3150 1300 3150
$Comp
L R R0
U 1 1 5645A377
P 2300 3100
F 0 "R0" V 2380 3100 40  0000 C CNN
F 1 "R" V 2307 3101 40  0000 C CNN
F 2 "~" V 2230 3100 30  0000 C CNN
F 3 "~" H 2300 3100 30  0000 C CNN
	1    2300 3100
	1    0    0    -1  
$EndComp
Text GLabel 2300 2850 1    60   Input ~ 0
V1
Connection ~ 2300 3350
$EndSCHEMATC
